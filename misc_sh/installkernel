#!/bin/sh
#
# This file is subject to the terms and conditions of the GNU General Public
# License.  See the file "COPYING" in the main directory of this archive
# for more details.
#
# Copyright (C) 1995 by Linus Torvalds
#
# Adapted from code in arch/i386/boot/Makefile by H. Peter Anvin
#
# "make install" script for i386 architecture
#
# Arguments:
#   $1 - kernel version
#   $2 - kernel image file
#   $3 - kernel map file
#   $4 - default install path (blank if root directory)
#

verify () {
	if [ ! -f "$1" ]; then
		echo ""                                                   1>&2
		echo " *** Missing file: $1"                              1>&2
		echo ' *** You need to run "make" before "make install".' 1>&2
		echo ""                                                   1>&2
		exit 1
 	fi
}

# Make sure the files actually exist
verify "$2"
verify "$3"
back_up () {
	if [ -f "$1" ]; then
		mv $1 $1.old
	fi
}

echo_instl_in () {
	echo $1 "installed in" $2
}
echo_linked_to () {
	echo $1 "linked to" $2
}

echo "install modules..."
make modules_install
echo "Done."
echo

cat $2 > $4/vmlinuz-$1 && echo_instl_in "Kernel Image" "$4/vmlinuz-$1"
back_up "$4/vmlinuz"
ln -sf $4/vmlinuz-$1 $4/vmlinuz && echo_linked_to "Kernel Image" "$4/vmlinuz"

cp $3 $4/System.map-$1 && echo_instl_in "System.map" "$4/System.map-$1"
back_up "$4/System.map"
ln -sf $4/System.map-$1 $4/System.map && echo_linked_to "System.map" "$4/System.map"

cp .config $4/config-$1 && echo_instl_in "Kernel config" "$4/config-$1"
back_up "$4/config"
ln -sf $4/config-$1 $4/config && echo_linked_to "Kernel config" "$4/config"
